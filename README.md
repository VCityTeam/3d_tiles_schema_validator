# 3D Tiles JSON Schema Validator

The validator reads B3DM 3DTiles and validates the tilesets and their extensions with JSON Schemas

- [x] Reads tilesets with Py3DTiles
- [x] Validates standard parts (tile, tileset, batch table and bounding volume)
- [ ] Validates extensions (see #1)

## Installation

On Windows:

```bash
git clone https://gitlab.com/VCityTeam/3d_tiles_schema_validator
cd 3d_tiles_schema_validator
python3 -m venv venv
. venv/Scripts/activate
(venv)$ pip install -e .
```

On Unix:

```bash
apt-get install virtualenv
git clone https://gitlab.com/VCityTeam/3d_tiles_schema_validator
cd 3d_tiles_schema_validator
virtualenv -p python3 venv
. venv/bin/activate
(venv)$ pip install -e .
```

## Use the validator

With the venv activated, run `validate` followed by the path(es) to the `tileset.json`:

```bash
(venv)$ validate ../my_tileset/tileset.json
```

```bash
(venv)$ validate ../my_tileset1/tileset.json ../my_tileset2/tileset.json ../my_tileset3/tileset.json
```

## Add an extension to validate

In `./jsonschemas/extensions`, create a new folder and add your JSON Schemas.

In `./validator`, create a Python script called `<extension_name>_extension_schemas.py`.

In this script, import `SchemaWithSample` and create one class per schema to validate. Also create a `<MyExtension>Schemas` class that appends all the schemas. Check the [Temporal extension](https://gitlab.com/VCityTeam/3d_tiles_schema_validator/-/blob/main/validator/temporal_extension_schemas.py) as example.

__Important__: each class inheriting from `SchemaWithSample` must have as [key](https://gitlab.com/VCityTeam/3d_tiles_schema_validator/-/blob/main/validator/schema_with_sample.py?ref_type=heads#L19) `<my_extension>_<ClassName>`, where:

- `<my_extension>` is the exact name of the extension as writen in the 3D Tiles (the JSON key in `extensions`)
- `<ClassName>` is the name of class created by Py3DTiles when reading the tileset (for example `BatchTable`, `Tile`)

In `schema_validators.py`, [register your extension](https://gitlab.com/VCityTeam/3d_tiles_schema_validator/-/blob/main/validator/schema_validators.py?ref_type=heads#L71)
