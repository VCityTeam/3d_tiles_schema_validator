# -*- coding: utf-8 -*-
import os
import re
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

requirements = (
    "jsonschema",
    "py3dtiles @ git+https://gitlab.com/py3dtiles/py3dtiles"
)

dev_requirements = (
    "pytest"
)

setup(
    name="3d_tiles_schema_validator",
    version="1.0",
    description="Python tool to validate 3D Tiles",
    long_description="",
    url="https://gitlab.com/VCityTeam/3d_tiles_schema_validator",
    author="VCityTeam",
    author_email="",
    license="",
    classifiers=[
        "Programming Language :: Python :: 3.9",
    ],
    packages=find_packages(),
    install_requires=requirements,
    test_suite="tests",
    extras_require={
        "dev": dev_requirements
    },
    entry_points={
        "console_scripts": ["validate=validator:main"],
    },
    include_package_data=True,
    package_data={"schema_validator": ["jsonschemas/*.json"]},
    zip_safe=False,
)
