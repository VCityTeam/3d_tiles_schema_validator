from .schema_with_sample import SchemaWithSample


class BatchTableHierarchySchemaWithSample(SchemaWithSample):

    def __init__(self):
        super().__init__('3DTILES_batch_table_hierarchy_BatchTable')
        self.set_directory('jsonschemas/extensions/batch_table_hierarchy')
        self.set_filename('3DTILES_batch_table_hierarchy.json')
        self.set_sample(
            {
                "classes": [],
                "instancesLength": 0,
                "classIds": [],
                "parentCounts": [],
                "parentIds": []
            }
        )


class BatchTableHierarchySchemas(list):
    def __init__(self, *args):
        list.__init__(self, *args)
        self.append(BatchTableHierarchySchemaWithSample())
