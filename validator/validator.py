import sys
from pathlib import Path

from py3dtiles.tileset import TileSet

from .schema_validators import SchemaValidators


def main():
    validators = SchemaValidators()
    for arg in sys.argv[1:]:
        path = Path(arg)
        tileset = TileSet.from_file(path)
        print('Tileset:', validators.validate(tileset.to_dict(), 'TileSet'))

        root_tile = tileset.root_tile
        print('Root Tile:', validators.validate(root_tile.to_dict(), 'Tile'))

        bbox = root_tile.bounding_volume
        print('Root Tile BBox:', validators.validate(
            bbox.to_dict(), 'BoundingVolumeBox'))

        for i, tile in enumerate(root_tile.get_all_children()):
            print('Tile_' + str(i) + ':', validators.validate(tile.to_dict(), 'Tile'))
            bbox = tile.bounding_volume
            print('Tile_' + str(i), 'BBox:',
                  validators.validate(bbox.to_dict(), 'BoundingVolumeBox'))
            tile_content = tile.get_or_fetch_content(tileset.root_uri)
            batch_table_data = tile_content.body.batch_table.header.data
            print('Tile_' + str(i), 'BatchTable:',
                  validators.validate(batch_table_data, 'BatchTable'))
